package com.hocrox.delivery.belgainwaffle.belapur.application;

import android.content.Context;
import android.location.Location;

import com.google.gson.Gson;

/**
 * created by sahilGoyal on 14/9/17.
 */

public class ApplicationConstants {
    public static Gson gson = new Gson();
    public static Context context;
    public static Location currentLocation;

    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static Boolean isActivityOpen = false;

    public static Boolean isFromFreindList = false;
    public static String latitude = "";
    public static String longitude = "";
    public static Boolean isChatActivityOpen = false;
    public static String baseApi ="http://192.168.0.25:8080/api/";

}
