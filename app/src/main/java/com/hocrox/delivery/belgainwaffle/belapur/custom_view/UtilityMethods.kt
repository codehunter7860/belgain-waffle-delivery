package com.hocrox.delivery.belgainwaffle.belapur.custom_view

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.net.ConnectivityManager
import android.provider.Settings
import android.support.v7.app.AlertDialog
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.Window
import android.widget.Toast

import com.hocrox.delivery.belgainwaffle.belapur.R

/**
 * Created by sahilgoyal on 15/9/17.
 */

class UtilityMethods {

    companion object {

        val utility: UtilityMethods = UtilityMethods()

    }

    var dialog_changed: Dialog? = null
    var progressDialog: ProgressDialog? = null

    fun isNetworkAvailable(context: Context): Boolean {
        try {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
             return activeNetworkInfo != null && activeNetworkInfo.isConnected
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return false
    }

    fun dismissDialog() {
        if (dialog_changed != null) {
            dialog_changed!!.dismiss()
        }
    }


    fun fromHtml(html: String): Spanned {
        val result: Spanned
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            result = Html.fromHtml(html)
        }
        return result
    }

    /*  fun showDialog(c: Context) {
          if (dialog_changed != null) {
              dialog_changed!!.dismiss()
          }
          dialog_changed = Dialog(c)
          dialog_changed!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
          dialog_changed!!.setContentView(R.layout.progressdialog)
          dialog_changed!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
          dialog_changed!!.setCancelable(false)
          dialog_changed!!.show()
      }*/

    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }


    fun showProgressDialog(context: Context, title: String, message: String) {


        if (dialog_changed != null) {
            dialog_changed!!.dismiss()
        }
        dialog_changed = Dialog(context)
        dialog_changed!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_changed!!.setContentView(R.layout.custom_progressdialog)
        dialog_changed!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog_changed!!.setCancelable(false)
        dialog_changed!!.show()


        /*  try {
              if (progressDialog != null) {
                  progressDialog!!.dismiss()
              }
              if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                  progressDialog = ProgressDialog(context)
              } else {
                  progressDialog = ProgressDialog(context, R.style.ProgressDialogStyle)

              }
              //progressDialog!!.setTitle(title)
              progressDialog!!.setContentView(R.layout.custom_progressdialog)
              progressDialog!!.setCancelable(false)
             // progressDialog!!.setMessage(message)
              progressDialog!!.show()
          } catch (e: Exception) {
              e.printStackTrace()
          }
  */
    }

    fun dismissProgressDialog() {
        /* try {
             if (progressDialog != null) {
                 progressDialog!!.dismiss()
             }
         } catch (e: Exception) {
             Log.e("progress problem", "yes")
         }*/
        try {
            if (dialog_changed != null) {
                dialog_changed!!.dismiss()
            }
        } catch (e: Exception) {
            Log.e("progress problem", "yes")
        }
    }


    fun isGPSEnabled(context: Context): Boolean {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (!enabled) {
            // showDialogGPS(context);
        }
        return enabled

    }

    private fun showDialogGPS(context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setCancelable(false)
        builder.setMessage("Do you want to enable your GPS?")
        builder.setPositiveButton("Enable") { _, _ ->
            context.startActivity(
                    Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
        builder.setNegativeButton("Ignore") { dialog, _ -> dialog.dismiss() }
        val alert = builder.create()
        alert.show()
    }


    fun sendAlertDialog(context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setCancelable(false)
        //  builder.setTitle("Enable GPS");
        builder.setMessage("Need Blood Today or Later?")
        builder.setPositiveButton("Today") { dialog, _ -> }
        builder.setNegativeButton("Later") { dialog, _ -> dialog.dismiss() }
        val alert = builder.create()
        alert.show()
    }


    fun shareApp(appUrl: String, context: Context) {
        try {
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Download App")
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, appUrl)
            context.startActivity(Intent.createChooser(sharingIntent, "Share RedLifeLine app via"))
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
