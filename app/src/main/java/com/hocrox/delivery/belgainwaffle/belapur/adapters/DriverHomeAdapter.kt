package com.hocrox.delivery.belgainwaffle.belapur.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hocrox.delivery.belgainwaffle.belapur.R
import com.hocrox.delivery.belgainwaffle.belapur.custom_view.CustomTextView

/**
 * Created by ashishkumar on 25/12/17.
 */

class DriverHomeAdapter : RecyclerView.Adapter<DriverHomeAdapter.DriverHomeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DriverHomeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.child_driver_home, parent, false)

        return DriverHomeViewHolder(view)
    }

    override fun onBindViewHolder(holder: DriverHomeViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 20
    }

    class DriverHomeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvAddress: CustomTextView
        var tvNavigate: CustomTextView
        var tvOrderNumber: CustomTextView
        var tvName: CustomTextView
        var tvMobileNumber: CustomTextView
        var tvKilometer: CustomTextView

        init {

            tvName = itemView.findViewById(R.id.tvName)
            tvOrderNumber = itemView.findViewById(R.id.tvOrderNumber)
            tvKilometer = itemView.findViewById(R.id.tvKilometer)
            tvAddress = itemView.findViewById(R.id.tvAddress)
            tvMobileNumber = itemView.findViewById(R.id.tvMobileNumber)
            tvNavigate = itemView.findViewById(R.id.tvNavigate)
        }
    }
}
