package com.hocrox.delivery.belgainwaffle.belapur.ServerRequests

import com.hocrox.delivery.belgainwaffle.belapur.application.ApplicationConstants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by sahilgoyal on 16/9/17.
 */
class ServiceGenerator {

    companion object {
        private val httpClient = OkHttpClient.Builder()

        internal var okHttpClient = httpClient.connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS).build()

       private var builder = Retrofit.Builder().addCallAdapterFactory(RxJavaCallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).baseUrl(ApplicationConstants.baseApi)

        fun <S> createService(serviceClass: Class<S>): S {
            val retrofit = builder.client(okHttpClient).build()
            return retrofit.create(serviceClass)
        }
    }
}