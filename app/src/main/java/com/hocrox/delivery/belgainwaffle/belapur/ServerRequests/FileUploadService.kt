package com.hocrox.delivery.belgainwaffle.belapur.ServerRequests

import com.google.gson.JsonElement
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

/**
 * Created by sahilgoyal on 16/9/17.
 */
interface FileUploadService {

/*    @Multipart
    @POST("v1/userProfile/uploadImage")
    fun uploadProfilePic(@Header("Authorization") token: String, @Part file: MultipartBody.Part): Observable<Response<UploadImageModel>>*/

    @Multipart
    @POST("v1/uploadDocument")
    fun uploadDocument(@Header("Authorization") token: String, @Part file: MultipartBody.Part): Call<JsonElement>


}


