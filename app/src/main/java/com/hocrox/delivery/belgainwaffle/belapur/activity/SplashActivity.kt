package com.hocrox.delivery.belgainwaffle.belapur.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.google.firebase.crash.FirebaseCrash
import com.hocrox.delivery.belgainwaffle.belapur.R
import java.lang.Exception

class SplashActivity : AppCompatActivity() {

    private val MY_REQUEST_CODE: Int = 1;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /* This method is used to get Exception or Error when occured while using app.
           You can see these on Firebase console or Email Address.*/
        FirebaseCrash.report(Exception());

        setContentView(R.layout.activity_splash)

        val runnable : Runnable = object : Runnable{
            override fun run() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION), MY_REQUEST_CODE)
                } else {
                    doSplash()
                }
            } else {

                doSplash()
            }
        }

        Handler().postDelayed(runnable, 1000);
    }

    private fun doSplash() {
        startActivity(Intent(this, NavigationActivity::class.java))
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == MY_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                doSplash()
            }
        }
    }
}
