package com.hocrox.delivery.belgainwaffle.belapur.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.view.View
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.hocrox.delivery.belgainwaffle.belapur.R
import com.hocrox.delivery.belgainwaffle.belapur.custom_view.CustomEditText
import com.hocrox.delivery.belgainwaffle.belapur.custom_view.CustomTextView

class LoginActivity : AppCompatActivity() {

    @BindView(R.id.tvLoginTitle)
    internal var tvLoginTitle: CustomTextView? = null
    @BindView(R.id.ivEmailTitle)
    internal var ivEmailTitle: ImageView? = null
    @BindView(R.id.etEmail)
    internal var etEmail: CustomEditText? = null
    @BindView(R.id.ivPasswordTitle)
    internal var ivPasswordTitle: ImageView? = null
    @BindView(R.id.etPassword)
    internal var etPassword: CustomEditText? = null
    @BindView(R.id.tvForgotPassword)
    internal var tvForgotPassword: CustomTextView? = null
    @BindView(R.id.cvLogin)
    internal var cvLogin: CardView? = null
    @BindView(R.id.tvLogin)
    internal var tvLogin: CustomTextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)
    }

    @OnClick(R.id.tvForgotPassword, R.id.tvLogin)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.tvForgotPassword -> {
            }
            R.id.tvLogin -> {
            }
        }
    }
}
