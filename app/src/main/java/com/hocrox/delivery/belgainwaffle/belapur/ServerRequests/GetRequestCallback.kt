package com.hocrox.delivery.belgainwaffle.belapur.ServerRequests

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.hocrox.delivery.belgainwaffle.belapur.custom_view.UtilityMethods
import com.hocrox.delivery.belgainwaffle.belapur.model.error_model.ErrorDTO

import retrofit2.Call
import retrofit2.Response

/**
 * Created by sahilgoyal on 28/8/17.
 */
class GetRequestCallback<T>(val dispatch: GetDispatch, val result: Response<T>, val resposneType: ResponseType, val context:Context) {


    fun onResponse() {

        UtilityMethods.utility.dismissProgressDialog()

        if (result.isSuccessful) {

            dispatch.apiSuccess(result.body(), resposneType)

        } else {


            val gson: Gson = GsonBuilder().create()
            val errorDTO: ErrorDTO = gson.fromJson(result.errorBody()?.string(), ErrorDTO::class.java)

            dispatch.apiError(errorDTO)
        }
    }

    fun onFailure(call: Call<T>?, t: Throwable?) {

        if (t != null) {
            dispatch.error(t.message)
        }

    }


}

