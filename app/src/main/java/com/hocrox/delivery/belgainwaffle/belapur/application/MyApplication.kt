package com.hocrox.delivery.belgainwaffle.belapur.application

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex

/**
 * Created by ashishkumar on 25/12/17.
 */

class MyApplication : Application() {
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

}
