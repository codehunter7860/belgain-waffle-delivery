package com.hocrox.delivery.belgainwaffle.belapur.custom_view

/*
  Created by ashishkumar on 28/04/16.
 */

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet

class CustomButton : android.support.v7.widget.AppCompatButton {

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context) : super(context) {
        init()
    }

    private fun init() {
        val tf = Typeface.createFromAsset(context.assets, "fonts/font.otf")
        typeface = tf
    }

}
