package com.hocrox.delivery.belgainwaffle.belapur.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.hocrox.delivery.belgainwaffle.belapur.R
import com.hocrox.delivery.belgainwaffle.belapur.adapters.ODTAdapter

class OnlineDriveThroughActivity : AppCompatActivity() {

    @BindView(R.id.ivBackArrow)
    internal var ivBackArrow: ImageView? = null
    @BindView(R.id.rvODTList)
    internal var rvODTList: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_online_drive_through)
        ButterKnife.bind(this)

        val driverHomeAdapter = ODTAdapter()

        rvODTList!!.layoutManager = LinearLayoutManager(this)
        rvODTList!!.adapter = driverHomeAdapter
        rvODTList!!.setHasFixedSize(false)
        rvODTList!!.itemAnimator = DefaultItemAnimator()
    }

    @OnClick(R.id.ivBackArrow)
    fun onViewClicked() {
        finish()
    }
}
