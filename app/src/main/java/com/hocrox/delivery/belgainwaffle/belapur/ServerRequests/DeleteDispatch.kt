package com.hocrox.delivery.belgainwaffle.belapur.ServerRequests

import com.hocrox.delivery.belgainwaffle.belapur.model.error_model.ErrorDTO

/**
 * Created by sahilgoyal on 28/8/17.
 */
interface DeleteDispatch {

    fun <T>apiDeleteSuccess(body: T,response: ResponseType)
    fun apiError(error: ErrorDTO)
    fun error(body: String?)
}