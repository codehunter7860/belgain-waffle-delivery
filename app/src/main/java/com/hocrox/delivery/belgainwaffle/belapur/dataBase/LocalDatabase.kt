package com.hocrox.delivery.belgainwaffle.belapur.dataBase;

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by sahilgoyal on 16/9/17.
 */
class LocalDatabase {

    private var USERTYPETABLE: String = "UserTable"
    private var USERTYPE: String = "UserType"
    private lateinit var sharedPreference: SharedPreferences

    private var USERTOKENTABLE: String = "UserDetailTable"
    private var USERTOKEN: String = "UserToken"
    private var USERDETAILTABLE: String = "UserDetail"

    private var COMPANYDETAILTABLE: String = "CompanyDetailTable"
    private var COMAPNYNAME: String = "CompanyName"
    private var COMPANYMOBILE: String = "CompanyMobile"
    private var COMPANYPROFILE: String = "CompanyProfile"
    private var COMPANYCOVER: String = "CompanyCover"
    private var COMPANYPROFILEID: String = "CompanyId"


    private var USERNAME: String = "UserName"
    private var USEREMAIL: String = "UserEmail"

    private var USERIMAGE: String = "UserImage"
    private var USERID: String = "ProfileId"


    companion object {

        var instance: LocalDatabase = LocalDatabase()

    }

    fun storeUserType(context: Context, value: String) {

        sharedPreference = context.getSharedPreferences(USERTYPETABLE, Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.putString(USERTYPE, value)
        editor.apply()
    }

    fun getUserType(context: Context): String? {

        sharedPreference = context.getSharedPreferences(USERTYPETABLE, Context.MODE_PRIVATE)
        return sharedPreference.getString(USERTYPE, "")
    }

    fun storeUserToken(context: Context, value: String) {

        sharedPreference = context.getSharedPreferences(USERTOKENTABLE, Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.putString(USERTOKEN, "Bearer " + value)
        editor.apply()
    }


    fun getUserToken(context: Context): String {

        sharedPreference = context.getSharedPreferences(USERTOKENTABLE, Context.MODE_PRIVATE)
        return sharedPreference.getString(USERTOKEN, "")
    }

    fun storeUserDetail(context: Context, value: String?, email: String?, image: String?, profileId: Int) {

        sharedPreference = context.getSharedPreferences(USERDETAILTABLE, Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.putString(USERNAME, value)
        editor.putString(USEREMAIL, email)
        editor.putString(USERIMAGE, image)
        editor.putInt(USERID, profileId)

        editor.apply()

    }

    fun storeUserFriends(context: Context, value: Int) {

        sharedPreference = context.getSharedPreferences("UserFriendsCount", Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.putInt("count", value)
        editor.apply()

    }

    fun getUserFriendCount(context: Context): Int {

        sharedPreference = context.getSharedPreferences("UserFriendsCount", Context.MODE_PRIVATE)
        return sharedPreference.getInt("count", 0)
    }


    fun storeCompanyDetail(context: Context, value: String?, email: String?, image: String?, profileId: Int) {

        sharedPreference = context.getSharedPreferences(COMPANYDETAILTABLE, Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.putString(COMAPNYNAME, value)
        editor.putString(COMPANYCOVER, email)
        editor.putString(COMPANYPROFILE, image)
        editor.putInt(COMPANYPROFILEID, profileId)

        editor.apply()

    }

    fun getCompanyId(context: Context): Int {

        sharedPreference = context.getSharedPreferences(COMPANYDETAILTABLE, Context.MODE_PRIVATE)
        return sharedPreference.getInt(COMPANYPROFILEID, 0)

    }

    fun getCompanyName(context: Context): String {

        sharedPreference = context.getSharedPreferences(COMPANYDETAILTABLE, Context.MODE_PRIVATE)
        return sharedPreference.getString(COMAPNYNAME, "")

    }

    fun getCompanyCover(context: Context): String {

        sharedPreference = context.getSharedPreferences(COMPANYDETAILTABLE, Context.MODE_PRIVATE)
        return sharedPreference.getString(COMPANYCOVER, "")

    }

    fun getCompanyImage(context: Context): String {

        sharedPreference = context.getSharedPreferences(COMPANYDETAILTABLE, Context.MODE_PRIVATE)
        return sharedPreference.getString(COMPANYPROFILE, "")

    }


    fun getUserId(context: Context): Int {

        sharedPreference = context.getSharedPreferences(USERDETAILTABLE, Context.MODE_PRIVATE)
        return sharedPreference.getInt(USERID, 0)

    }


    fun clearUserDetail(context: Context) {

        sharedPreference = context.getSharedPreferences(USERDETAILTABLE, Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.clear()
        editor.apply()
    }


    fun clearUserToken(context: Context) {

        sharedPreference = context.getSharedPreferences(USERTOKENTABLE, Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.clear()
        editor.apply()
    }

    fun clearUserType(context: Context) {

        sharedPreference = context.getSharedPreferences(USERTYPETABLE, Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.clear()
        editor.apply()
    }

    fun getUserName(context: Context): String {

        sharedPreference = context.getSharedPreferences(USERDETAILTABLE, Context.MODE_PRIVATE)
        return sharedPreference.getString(USERNAME, "")

    }

    fun getUserImage(context: Context): String {

        sharedPreference = context.getSharedPreferences(USERDETAILTABLE, Context.MODE_PRIVATE)
        return sharedPreference.getString(USERIMAGE, "")

    }

    fun getUserEmail(context: Context): String {

        sharedPreference = context.getSharedPreferences(USERDETAILTABLE, Context.MODE_PRIVATE)
        return sharedPreference.getString(USEREMAIL, "")

    }

    fun clearUserData(context: Context) {

        val sharedPreference: SharedPreferences = context.getSharedPreferences(COMPANYDETAILTABLE, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.clear()
        editor.apply()

        val sharedPreference1: SharedPreferences = context.getSharedPreferences(USERDETAILTABLE, Context.MODE_PRIVATE)
        val editor1: SharedPreferences.Editor = sharedPreference1.edit()
        editor1.clear()
        editor1.apply()

        val sharedPreference2: SharedPreferences = context.getSharedPreferences(USERTOKENTABLE, Context.MODE_PRIVATE)
        val editor2: SharedPreferences.Editor = sharedPreference2.edit()
        editor2.clear()
        editor2.apply()

        val sharedPreference3: SharedPreferences = context.getSharedPreferences(USERTYPETABLE, Context.MODE_PRIVATE)
        val editor3: SharedPreferences.Editor = sharedPreference3.edit()
        editor3.clear()
        editor3.apply()
    }


}

