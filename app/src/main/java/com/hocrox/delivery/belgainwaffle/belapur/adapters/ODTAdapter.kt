package com.hocrox.delivery.belgainwaffle.belapur.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hocrox.delivery.belgainwaffle.belapur.R
import com.hocrox.delivery.belgainwaffle.belapur.custom_view.CustomTextView

/**
 * Created by ashishkumar on 25/12/17.
 */

class ODTAdapter : RecyclerView.Adapter<ODTAdapter.ODTViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ODTViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.child_online_drive_thru, parent, false)
        return ODTViewHolder(view)
    }

    override fun onBindViewHolder(holder: ODTViewHolder, position: Int) {}

    override fun getItemCount(): Int {
        return 20
    }

    class ODTViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvAddress: CustomTextView
        var tvNavigate: CustomTextView
        var tvName: CustomTextView
        var tvOrderNumber: CustomTextView
        var tvKilometer: CustomTextView
        var tvMobileNumber: CustomTextView

        init {

            tvAddress = itemView.findViewById(R.id.tvAddress)
            tvNavigate = itemView.findViewById(R.id.tvNavigate)
            tvName = itemView.findViewById(R.id.tvName)
            tvOrderNumber = itemView.findViewById(R.id.tvOrderNumber)
            tvKilometer = itemView.findViewById(R.id.tvKilometer)
            tvMobileNumber = itemView.findViewById(R.id.tvMobileNumber)
        }
    }
}
