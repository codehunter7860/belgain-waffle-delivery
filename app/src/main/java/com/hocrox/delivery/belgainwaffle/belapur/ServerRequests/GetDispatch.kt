package com.hocrox.delivery.belgainwaffle.belapur.ServerRequests

import com.hocrox.delivery.belgainwaffle.belapur.model.error_model.ErrorDTO


/**
 * Created by sahilgoyal on 28/8/17.
 */
interface GetDispatch {

    fun <T>apiSuccess(body: T,response: ResponseType)
    fun apiError(error: ErrorDTO)
    fun error(body: String?)
}