package com.hocrox.delivery.belgainwaffle.belapur.activity

import android.graphics.Point
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import com.ahmadrosid.lib.drawroutemap.DrawMarker
import com.ahmadrosid.lib.drawroutemap.DrawRouteMaps
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.hocrox.delivery.belgainwaffle.belapur.R
import com.hocrox.delivery.belgainwaffle.belapur.custom_view.CustomTextView

class NavigationActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null

    private var tbToolbar: Toolbar? = null;
    private var tvToolbarTitle: CustomTextView? = null;
    private var dlLayout: DrawerLayout? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation);

        tbToolbar = findViewById(R.id.tbToolbar) as Toolbar;
        tvToolbarTitle = findViewById(R.id.tvToolbarTitle) as CustomTextView;
        dlLayout = findViewById(R.id.dlLayout) as DrawerLayout;

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tbToolbar?.elevation = 5f
        }

        tvToolbarTitle!!.text = resources.getString(R.string.navigate_order)
        tvToolbarTitle!!.setTextColor(ContextCompat.getColor(this, R.color.colorBrownLogin))

        creatDrawer()

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun creatDrawer() {
        val actionBarDrawerToggle = object : ActionBarDrawerToggle(this, dlLayout, tbToolbar, R.string.opendrawer, R.string.closedrawer) {
            override fun onDrawerOpened(drawerView: View?) {
                super.onDrawerOpened(drawerView)
            }

            override fun onDrawerClosed(drawerView: View?) {
                super.onDrawerClosed(drawerView)
            }
        }

        dlLayout?.addDrawerListener(actionBarDrawerToggle)

        actionBarDrawerToggle.syncState()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.mMap = googleMap

        val origin = LatLng(28.5795, 77.3136)
        val destination = LatLng(30.7114, 76.6900)

        DrawRouteMaps.getInstance(this).draw(origin, destination, mMap)

        DrawMarker.getInstance(this).draw(mMap!!, origin, R.drawable.delivery_boy, "Origin Location")
        DrawMarker.getInstance(this).draw(mMap!!, destination, R.drawable.pointer, "Destination Location")

        val bounds = LatLngBounds.Builder().include(origin).include(destination).build()

        val displaySize = Point()

        windowManager.defaultDisplay.getSize(displaySize)

        mMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30))
        mMap!!.setMaxZoomPreference(10f)
    }
}
