package com.hocrox.delivery.belgainwaffle.belapur.ServerRequests

import com.hocrox.delivery.belgainwaffle.belapur.application.ApplicationConstants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by sahilgoyal on 28/8/17.
 */
class DispatchResponse {

    companion object {

        fun dispatchResponse(dispatchs: Dispatch, type: ResponseType, objects: Any?) {

            val http: HttpLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

            val builder: OkHttpClient.Builder = OkHttpClient.Builder()

            builder.addInterceptor(http)
            val retrofit: Retrofit = Retrofit.Builder().addCallAdapterFactory(RxJavaCallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).baseUrl(ApplicationConstants.baseApi).build()

            val postRequest: PostRequests = retrofit.create(PostRequests::class.java)


            when (type) {


                ResponseType.LOGIN -> {

                    /*val sendLoginRequestModel: SendLoginData = objects as SendLoginData
                    postRequest.authenticateUser(sendLoginRequestModel).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                        RequestCallback(dispatchs, result).onResponse()
                    }, { error ->

                        UtilityMethods.utility.dismissProgressDialog()
                        UtilityMethods.utility.showToast(ApplicationConstants.context, "" + error.message)

                    })*/

                }
                ResponseType.REGISTER -> {

                    /*val sendUserData: SendUserData = objects as SendUserData
                    postRequest.registerUser(sendUserData).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                        RequestCallback(dispatchs, result).onResponse()
                    }, { error ->
                        UtilityMethods.utility.dismissProgressDialog()
                        UtilityMethods.utility.showToast(ApplicationConstants.context, "" + error.message)

                    })*/

                }
                ResponseType.ADDADDRESS -> {

                    /*val sendUserData: AddAddress = objects as AddAddress
                    postRequest.addAddress(sendUserData).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                        RequestCallback(dispatchs, result).onResponse()
                    }, { error ->
                        UtilityMethods.utility.dismissProgressDialog()
                        UtilityMethods.utility.showToast(ApplicationConstants.context, "" + error.message)

                    })
*/
                }
                ResponseType.CREATEORDER -> {


                    /*if (LocalDatabase.instance.getUserToken(ApplicationConstants.context) != "") {

                        postRequest.createOrder(LocalDatabase.instance.getUserToken(ApplicationConstants.context), objects as CreateWaffleOder).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                            RequestCallback(dispatchs, result).onResponse()
                        }, { error ->
                            UtilityMethods.utility.dismissProgressDialog()
                            UtilityMethods.utility.showToast(ApplicationConstants.context, "" + error.message)

                        })

                    }*/
                }

            }

        }

    }

}




