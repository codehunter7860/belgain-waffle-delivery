package com.hocrox.delivery.belgainwaffle.belapur.ServerRequests

import com.hocrox.delivery.belgainwaffle.belapur.application.ApplicationConstants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by sahilgoyal on 28/8/17.
 */
class DispatchGetResponse {

    companion object {

        val baseApi: String = "http://45.56.73.202:8097/api/"
//  val baseApi: String = "http://192.168.0.12:8097/api/"


        fun dispatchResponse(dispatchs: GetDispatch, type: ResponseType, objects: Any?) {


            val http: HttpLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

            val builder: OkHttpClient.Builder = OkHttpClient.Builder()

            builder.addInterceptor(http)
            val retrofit: Retrofit = Retrofit.Builder().addCallAdapterFactory(RxJavaCallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).baseUrl(ApplicationConstants.baseApi).build()

            val getRequest: GetRequests = retrofit.create(GetRequests::class.java)


            when (type) {

                ResponseType.GETCATEGORY -> {

                    /*if (LocalDatabase.instance.getUserToken(ApplicationConstants.context) != "") {

                        getRequest.getCategoories(LocalDatabase.instance.getUserToken(ApplicationConstants.context)).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                            UtilityMethods.utility.dismissProgressDialog()

                            GetRequestCallback(dispatchs, result, type).onResponse()
                        }, { error ->
                            UtilityMethods.utility.showToast(ApplicationConstants.context, "" + error.message)
                            UtilityMethods.utility.dismissProgressDialog()
                        })
                    }*/
                }

                ResponseType.GETCATEGORIESITEM -> {

                    /*if (LocalDatabase.instance.getUserToken(ApplicationConstants.context) != "") {

                        getRequest.getCategoriesItem(LocalDatabase.instance.getUserToken(ApplicationConstants.context),objects.toString().toInt()).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                            GetRequestCallback(dispatchs, result, type).onResponse()
                        }, { error ->
                            UtilityMethods.utility.showToast(ApplicationConstants.context, "" + error.message)
                            UtilityMethods.utility.dismissProgressDialog()
                        })
                    }*/
                }
                ResponseType.GETCATEGORYITEMDETAIL -> {

                    /*if (LocalDatabase.instance.getUserToken(ApplicationConstants.context) != "") {

                        getRequest.getCategoryItemDetail(LocalDatabase.instance.getUserToken(ApplicationConstants.context),objects.toString().toInt()).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                            GetRequestCallback(dispatchs, result, type).onResponse()
                        }, { error ->
                            UtilityMethods.utility.showToast(ApplicationConstants.context, "" + error.message)
                            UtilityMethods.utility.dismissProgressDialog()
                        })
                    }*/
                }
                ResponseType.GETADDRESSES -> {

                    /*if (LocalDatabase.instance.getUserToken(ApplicationConstants.context) != "") {
                        getRequest.getAddresses(LocalDatabase.instance.getUserToken(ApplicationConstants.context)).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                            GetRequestCallback(dispatchs, result, type).onResponse()
                        }, { error ->
                            UtilityMethods.utility.showToast(ApplicationConstants.context, "" + error.message)
                            UtilityMethods.utility.dismissProgressDialog()
                        })
                    }*/
                }
                ResponseType.GETONEADDRESS -> {

                    /*if (LocalDatabase.instance.getUserToken(ApplicationConstants.context) != "") {
                        getRequest.getOneAddress(LocalDatabase.instance.getUserToken(ApplicationConstants.context),objects.toString().toInt()).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                            GetRequestCallback(dispatchs, result, type).onResponse()
                        }, { error ->
                            UtilityMethods.utility.showToast(ApplicationConstants.context, "" + error.message)
                            UtilityMethods.utility.dismissProgressDialog()
                        })
                    }*/
                }



            }

        }
    }
}




