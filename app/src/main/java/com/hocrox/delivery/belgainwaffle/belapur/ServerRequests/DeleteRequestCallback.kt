package com.hocrox.delivery.belgainwaffle.belapur.ServerRequests

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.hocrox.delivery.belgainwaffle.belapur.custom_view.UtilityMethods
import com.hocrox.delivery.belgainwaffle.belapur.model.error_model.ErrorDTO
import retrofit2.Call
import retrofit2.Response

/**
 * Created by sahilgoyal on 28/8/17.
 */
class DeleteRequestCallback<T>(val dispatch: DeleteDispatch, val result: Response<T>, val resposneType: ResponseType, val context:Context) {


    fun onResponse() {

        UtilityMethods.utility.dismissProgressDialog()

        if (result.isSuccessful) {

            dispatch.apiDeleteSuccess(result.body(), resposneType)

        } else {


            val gson: Gson = GsonBuilder().create()
            val errorDTO: ErrorDTO = gson.fromJson(result.errorBody()?.string(), ErrorDTO::class.java)
            if (errorDTO.fieldErrors != null) {
                val message = errorDTO.fieldErrors.get(0).message
                val field = errorDTO.fieldErrors.get(0).field
                Log.e("saddsdsa", "" + errorDTO.fieldErrors.get(0))

                UtilityMethods.utility.showToast(context, field + ": " + message)

            } else {
                UtilityMethods.utility.showToast(context, errorDTO.description)
                Log.e("saddsdsa", "" + errorDTO.description)
            }
            dispatch.apiError(errorDTO)
        }
    }

    fun onFailure(call: Call<T>?, t: Throwable?) {

        if (t != null) {
            dispatch.error(t.message)
        }

    }


}