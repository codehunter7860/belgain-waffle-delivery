package com.hocrox.delivery.belgainwaffle.belapur.ServerRequests

import com.google.gson.annotations.SerializedName
import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter

/**
 * Created by sahilgoyal on 29/9/17.
 */
@Getter
@Setter
@NoArgsConstructor
class SaveChatMessage{

    @SerializedName("result")
    var content: List<Content>? = null
    @SerializedName("totalElements")
    var totalElements: Int = 0
    @SerializedName("totalPages")
    var totalPages: Int = 0
    @SerializedName("last")
    var last: Boolean = false
    @SerializedName("numberOfElements")
    var numberOfElements: Int = 0
    @SerializedName("sort")
    var sort: String? = null
    @SerializedName("first")
    var first: Boolean = false
    @SerializedName("size")
    var size: Int = 0
    @SerializedName("number")
    var number: Int = 0

    @Getter
    @Setter
    @NoArgsConstructor
    class Sender {
        @SerializedName("id")
        var id: Int = 0
        @SerializedName("password")
        var password: String? = null
        @SerializedName("username")
        var username: String? = null
        @SerializedName("email")
        var email: String? = null
        @SerializedName("activated")
        var activated: Boolean = false
        @SerializedName("token")
        var token: String? = null
    }

    @Getter
    @Setter
    @NoArgsConstructor
    class Content {
        @SerializedName("chatId")
        var chatId: Int = 0
        @SerializedName("message")
        var message: String? = null
        @SerializedName("sender")
        var sender: Sender? = null
        @SerializedName("sentOn")
        var sentOn: String? = null
        @SerializedName("senderImage")
        var senderImage: String? = null
    }
}