package com.hocrox.delivery.belgainwaffle.belapur.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.hocrox.delivery.belgainwaffle.belapur.R
import com.hocrox.delivery.belgainwaffle.belapur.adapters.DriverHomeAdapter

class DriverHomeActivity : AppCompatActivity() {

    @BindView(R.id.ivBackArrow)
    internal var ivBackArrow: ImageView? = null
    @BindView(R.id.rvDriverHomeList)
    internal var rvDriverHomeList: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_home)
        ButterKnife.bind(this)

        val driverHomeAdapter = DriverHomeAdapter()

        rvDriverHomeList!!.layoutManager = LinearLayoutManager(this)
        rvDriverHomeList!!.adapter = driverHomeAdapter
        rvDriverHomeList!!.setHasFixedSize(false)
        rvDriverHomeList!!.itemAnimator = DefaultItemAnimator()
    }

    @OnClick(R.id.ivBackArrow)
    fun onViewClicked() {
        finish()
    }
}
