package com.hocrox.delivery.belgainwaffle.belapur.custom_view

import android.content.Context
import android.support.v7.widget.LinearLayoutManager

public class CustomLinearLayout: LinearLayoutManager {

constructor(context:Context) : super(context) {

}

    override fun canScrollVertically(): Boolean {
        return false
    }
}