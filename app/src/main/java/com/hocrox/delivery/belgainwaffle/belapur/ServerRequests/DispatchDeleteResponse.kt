package com.hocrox.delivery.belgainwaffle.belapur.ServerRequests

import com.hocrox.delivery.belgainwaffle.belapur.application.ApplicationConstants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by sahilgoyal on 28/8/17.
 */
class DispatchDeleteResponse {

    companion object {

        //  val baseApi: String = "http://192.168.0.9:8097/api/"


        fun dispatchResponse(dispatchs: DeleteDispatch, type: ResponseType, objects: Any?) {


            val http: HttpLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

            val builder: OkHttpClient.Builder = OkHttpClient.Builder()

            builder.addInterceptor(http)
            val retrofit: Retrofit = Retrofit.Builder().addCallAdapterFactory(RxJavaCallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).baseUrl(ApplicationConstants.baseApi).build()

            val deleteRequest: DeleteRequests = retrofit.create(DeleteRequests::class.java)


            when (type) {

             /*   ResponseType.DELETE_FRIEND -> {
*//*
                    if (LocalDatabase.instance.getUserToken(ApplicationConstants.context) != "") {

                        deleteRequest.deleteFriend(LocalDatabase.instance.getUserToken(ApplicationConstants.context), objects.toString().toInt()).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
                            DeleteRequestCallback(dispatchs, result, type).onResponse()
                        }, { error ->
                            Log.e("teet", "error dto" + error.message)
                            UtilityMethods.utility.showToast(ApplicationConstants.context, "" + error.message)
                            UtilityMethods.utility.dismissProgressDialog()
                        })
                    }*//*
                }*/
            }
        }
    }
}