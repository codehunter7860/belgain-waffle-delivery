package com.hocrox.delivery.belgainwaffle.belapur.custom_view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class CustomSeekBar extends View {
    int Progress;
    int widthPixels;
    int heightPixels;
    private ArrayList<ProgressItem> mProgressItemsList = new ArrayList<>();

    public CustomSeekBar(Context context) {
        super(context);
    }

    public CustomSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void initData(ArrayList<ProgressItem> progressItemsList) {
        this.mProgressItemsList = progressItemsList;
    }

    public void setProgress(int progress) {

        this.Progress = progress;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);

        widthPixels = displayMetrics.widthPixels;
        heightPixels = displayMetrics.heightPixels;
        Log.e("testing 2", "called");

    }


    @Override
    protected synchronized void onMeasure(int widthMeasureSpec,
                                          int heightMeasureSpec) {
        // TODO Auto-generated method stub
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.e("testing ", "called" + getWidth() + ">>>" + getHeight() + ">>" + canvas.getHeight());
        if (Progress >= 33) {
            int lineTo = (getWidth() * 33) / 100;

            Paint fgPaintSel = new Paint();
            fgPaintSel.setAlpha(255);
            fgPaintSel.setStrokeWidth(70);
            fgPaintSel.setDither(true);                    // set the dither to true
            fgPaintSel.setColor(Color.CYAN);
            fgPaintSel.setStrokeJoin(Paint.Join.ROUND);    // set the join to round you want
            fgPaintSel.setStrokeJoin(Paint.Join.ROUND);    // set the join to round you want
            fgPaintSel.setStyle(Paint.Style.FILL_AND_STROKE);

            fgPaintSel.setPathEffect(new DashPathEffect(new float[]{8, 8}, 50));
            Path baseline = new Path();
            baseline.moveTo(0, 0);
            baseline.lineTo(lineTo, 0);
            canvas.drawPath(baseline, fgPaintSel);

            Paint nextPaintSelection = new Paint();
            nextPaintSelection.setAlpha(255);
            nextPaintSelection.setStrokeWidth(70);
            nextPaintSelection.setColor(Color.BLUE);
            nextPaintSelection.setStyle(Paint.Style.FILL_AND_STROKE);
            nextPaintSelection.setPathEffect(new DashPathEffect(new float[]{8, 8}, 50));

            if (Progress >= 66) {

                int lineToNext = (getWidth() * 66) / 100;
                Path baseline1 = new Path();
                baseline1.moveTo(lineTo, 0);
                baseline1.lineTo(lineToNext, 0);
                canvas.drawPath(baseline1, nextPaintSelection);


                Paint lastPaintSelection = new Paint();
                lastPaintSelection.setAlpha(255);
                lastPaintSelection.setStrokeWidth(70);
                lastPaintSelection.setColor(Color.RED);
                lastPaintSelection.setStyle(Paint.Style.FILL_AND_STROKE);
                lastPaintSelection.setPathEffect(new DashPathEffect(new float[]{8, 8}, 50));
                int lineToLast = (getWidth() * Progress) / 100;
                Path path = new Path();
                path.moveTo(lineToNext, 0);
                path.lineTo(lineToLast, 0);
                canvas.drawPath(path, lastPaintSelection);

            } else {
                int lineToNext = (getWidth() * Progress) / 100;

                Path baseline2 = new Path();
                baseline2.moveTo(lineTo, 0);
                baseline2.lineTo(lineToNext, 0);
                canvas.drawPath(baseline2, nextPaintSelection);
            }


        } else {

            Paint fgPaintSel = new Paint();
            fgPaintSel.setAlpha(255);
            fgPaintSel.setStrokeWidth(70);
            fgPaintSel.setColor(Color.CYAN);
            fgPaintSel.setStyle(Paint.Style.FILL_AND_STROKE);
            fgPaintSel.setPathEffect(new DashPathEffect(new float[]{8, 8}, 50));
            Path baseline = new Path();
            baseline.moveTo(0, 0);
            baseline.lineTo(Progress, 0);
            canvas.drawPath(baseline, fgPaintSel);

        }

    }

  /*  private int[] getRainbowColors() {
        int[] colors = getResources().getIntArray(R.array.itemcolor);
        return colors;
    }*/

}